package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	csvFilename := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
	flag.Parse()

	//_ = csvFilename

	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("failed to open the CSV file: %s \n", *csvFilename))
	}
	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		exit("Failed to parse the provided CSV file")
	}
	problems := parseLines(lines)

	correct := 0
	for i, p := range problems {
		//the index is zero based, so we add 1 to it
		fmt.Printf("Problem #%d: %s = \n", i+1, p.q)
		var answer string
	//	We use scanf we helped us remove spaces, dont use it if you are expecting a string, so it does not excape spaces inbetween.
	fmt.Scanf("%s\n", &answer)
		if answer == p.a {
			correct++
			fmt.Println("Correct!")
		} else {
			fmt.Println("InCorrect!")
		}
	}
	fmt.Printf("You scroed %d out of %d.\n", correct, len(problems))
}

func parseLines(lines [][]string) []problem {
	ret := make([]problem, len(lines))
	for i, line := range lines {
		ret[i] = problem{
			q: line[0],
			a: strings.TrimSpace(line[1]),
		}
	}
	return ret
}

type problem struct {
	q string
	a string
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
